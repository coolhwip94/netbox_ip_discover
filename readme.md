# Netbox IP Discover
> This repo will a project that discovers IP addresses locally and updates netbox

## Setup
---
- This repo uses an `.env` file for loading variables
> Here is an example
```
subnet_ip = 10.1.0.0/16
netbox_token = abcdefgexampletoken
netbox_url = 10.2.1.1
```
- Python Setup
```
# Create Virtual env
python3 -m venv venv

# Activation venv
source venv/bin/activate

# Install requirements
pip3 install -r requirements.txt

# Run Script
python3 main.py
```


## Scripts
---
- `main.py`
  - This will scan all IPv4 Addresses within given subnet and post them to netbox
  - Reachable IPv4 will be posted as `reserved`
  - Unreachable IPv4 will be posted as `active`


