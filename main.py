from netaddr import IPNetwork
import os
import logging
import json
import requests
from dotenv import load_dotenv
import sys

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger('main')
load_dotenv()
netbox_url = os.getenv('netbox_url')
netbox_token = os.getenv('netbox_token')


# checkfs if ip is active or not
def check_ip(ipv4_list):
    '''
    Pings every ipv4 in ipv4_list and checks for reachability
    Params:
      ipv4_list (list): List of IPv4 addresses without CIDR suffix

    Return:
      ip_dict (dict) : dict with response results, key = ipv4 value = True/False
    '''
    # creat dictionary to populate results
    ip_dict = {}
    for ipv4 in ipv4_list:
        try:
            response = os.system('ping -c 1 -W 2 ' + ipv4)

            if response == 0:
                # this means ip is in use
                ip_dict[ipv4] = True
                logger.info(f"{ipv4} is in use.")
            else:
                # this means ip is NOT in use
                ip_dict[ipv4] = False
                logger.info(f"{ipv4} is NOT in use.")

        except Exception:
            logger.exception("Failed to process IP list")

    return ip_dict


def add_ip_netbox(ipv4_address, token, status="active"):
    '''
    Add IPv4 Address to Netbox Instance
    Params:
      ipv4_address (str) : IPv4 address with cidr ex: 10.1.0.0/16
      token (str) : Netbox auth token
      status (str) : active, reserved, deprecated, dhcp, slaac

    Return:
       requests.models.Response

    '''
    headers = {
        'accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': f'Token {token}',
    }

    data = {"address": ipv4_address,  "status": status}
    try:
        response = requests.post(
            f'http://{netbox_url}/api/ipam/ip-addresses/', headers=headers, data=json.dumps(data))
    except Exception:
        logger.exception("Failed to post IPv4 to Netbox")

    return response


if __name__ == "__main__":

    # Home Network Subet
    subnet_ip = os.getenv('subnet_ip')
    subnet = IPNetwork(subnet_ip)
    cidr = subnet_ip.split("/")[-1]

    # Grab all IPs in subnet
    all_ips = [str(ip) for ip in subnet.iter_hosts()]

    ip_dict = check_ip(ipv4_list=all_ips)

    # reserved refers to IPv4 that responds to ping
    for ipv4, reserved in ip_dict.items():

        # add /24 cidr to ip
        ipv4_address = ipv4 + f"/{cidr}"

        status = "reserved" if reserved else "active"
        
        # send to netbox based on reserved status
        response = add_ip_netbox(ipv4_address, token=netbox_token, status=status)
        
        logger.info(
            f"Add {ipv4_address} to Netbox Status Code : {response.status_code}")
